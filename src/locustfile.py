# import module that helps to choose random items
import random
# import modules from locust
from locust import HttpLocust, TaskSet, task

# define a dictionary with lists views
# it contains name and url
URLS = {
    'index': '/',
    'review_list': '/review/',
    'review_detail': ['/review/mr-fingers-outer-acid-ep/', '/review/el-guincho-hiperasia/', '/review/rebeka-davos/',
                      '/review/matmos-ultimate-care-ii/', '/review/wild-nothing-life-pause/',
                      '/review/1975-i-it-when-you-sleep-you-are-so-beautiful-yet-/',
                      '/review/animal-collectiive-painting/', '/review/adrian-younge-something-about-april-ii/',
                      '/review/mr-fingers-outer-acid-ep/', '/review/mr-fingers-outer-acid-ep/',
                      '/review/mr-fingers-outer-acid-ep/'],
    'playlist_list': '/playlist/',
    'playlist_detail': ['/playlist/susanna-hole/', '/playlist/pro8l3m-2040/', '/playlist/phoebe-ryan-chronic/',
                        '/playlist/gang-albanii-albanskie-kakao/', '/playlist/reni-jusis-bejbi-siter/',
                        '/playlist/maria-peszek-modern-holocaust/', '/playlist/chris-cohen-torrey-pine/',
                        '/playlist/james-blake-modern-soul/', '/playlist/susanna-hole/', '/playlist/susanna-hole/',
                        '/playlist/susanna-hole/'],
    'short_song_list': '/short/song/',
    'short_song_detail': ['/short/song/lido-life-peder/',
                          '/short/song/jessy-lanza-vv-violence/',
                          '/short/song/anno-domini-chrzest-polski-966/',
                          '/short/song/mark-pritchard-feat-thom-yorke-beautiful-people/',
                          '/ranking/100-plyt-1990-1999/',
                          '/short/song/leon-vynehall-beau-sovereign/',
                          '/short/song/novelty-daughter-not-fair/',
                          '/short/song/lona-i-webber-nie-mam-pojecia/',
                          '/short/song/anohni-drone-bomb-me/',
                          '/short/song/miguel-waves-tame-impala-remix/',
                          '/short/song/hey-predko-predzej/'],
    'short_album_list': '/short/album/',
    'draft_list': '/draft/',
    'draft_detail': ['/draft/archy-marshall-new-place-2-drown/', '/draft/lif-new-mini-ep/',
                     '/draft/eugeniusz-rudnik-erdada-na-tasme/', '/draft/darkside-psychic/',
                     '/draft/james-holden-the-inheritors/', '/draft/robert-glasper-black-radio/',
                     '/draft/andy-stott-luxury-problems/', '/draft/white-lung-sorry/', '/draft/wild-nothing-nocturne/',
                     '/draft/friendly-fires-pala/', '/draft/gotye-making-mirrors/',
                     '/draft/leyland-kirby-eager-to-tear-apart-the-stars/', '/draft/david-lynch-crazy-clown-time/',
                     '/draft/dannielradall-pardo/', '/draft/kido-yoji-call-a-romance-ep/', '/draft/sebastian-total/',
                     '/draft/asobi-seksu-fluorescence/', '/draft/lou-reed-metallica-lulu/',
                     '/draft/noel-gallaghers-high-flying-birds-noel-gallaghers/',
                     '/draft/ben-westbeech-theres-more-to-life-than-this/', '/draft/sea-and-cake-moonlight-butterfly/',
                     '/draft/paramore-brand-new-eyes/', '/draft/blow-jesli-czujesz/', '/draft/daughters-daughters/',
                     '/draft/walkmen-lisbon/', '/draft/eldo-zapiski-z-1001-nocy/',
                     '/draft/krallice-dimensional-bleedthrough/', '/draft/benoit-pioulard-lasted/',
                     '/draft/thermals-personal-life/', '/draft/leon-somov-feat-jazzu-offline-ep/'],
    'ranking_list': '/ranking/',
    'ranking_detail': ['/ranking/10-najlepszych-plyt-2015/', '/ranking/20-najlepszych-singli-2015/',
                       '/ranking/najlepsze-polskie-single-xx-wieku/', '/ranking/100-najlepszych-plyt-2010-2014/',
                       '/ranking/100-najlepszych-singli-2010-2014/', '/ranking/specjalne-polskie-piosenki/',
                       '/ranking/10-najlepszych-plyt-2014/', '/ranking/20-najlepszych-singli-2014/',
                       '/ranking/najlepsze-polskie-plyty-xx-wieku/', '/ranking/20-najlepszych-plyt-2013/',
                       '/ranking/20-najlepszych-singli-2013/', '/ranking/50-najlepszych-nagran-2012/',
                       '/ranking/100-plyt-1990-1999/', '/ranking/100-singli-1990-1999/',
                       '/ranking/20-najlepszych-singli-2011/', '/ranking/10-najlepszych-plyt-2011/',
                       '/ranking/20-najlepszych-singli-2010/', '/ranking/10-najlepszych-plyt-2010/',
                       '/ranking/100-plyt-2000-2009-na-swiecie/', '/ranking/100-singli-2000-2009-na-swiecie/',
                       '/ranking/100-polskich-plyt-2000-2009/', '/ranking/100-polskich-singli-2000-2009/',
                       '/ranking/20-najlepszych-singli-2009/', '/ranking/10-najlepszych-plyt-2009/',
                       '/ranking/20-najlepszych-singli-2008/', '/ranking/10-najlepszych-plyt-2008/',
                       '/ranking/10-najlepszych-plyt-2007/', '/ranking/20-najlepszych-singli-2007/',
                       '/ranking/20-najlepszych-singli-2006/', '/ranking/10-najlepszych-plyt-2006/'],
    'interview_list': '/interview/',
    'interview_detail': ['/interview/wywiad-z-igorem-pudlo/', '/interview/wywiad-z-phantom/',
                         '/interview/wywiad-z-torche/', '/interview/wywiad-z-necks/',
                         '/interview/wywiad-z-jazzpospolita/',
                         '/interview/wywiad-z-ksiezycem/', '/interview/wywiad-z-danielem-drumzem/',
                         '/interview/wywiad-z-lone/', '/interview/wywiad-z-forest-swords/',
                         '/interview/wywiad-z-mikiem-scheidtem-z-yob/', '/interview/wywiad-z-chloe-martini/',
                         '/interview/wywiad-z-brettem-campbellem-z-pallbearer/',
                         '/interview/wywiad-z-pionalem-off-festival-2014/',
                         '/interview/wywiad-z-noonem-off-festival-2014/',
                         '/interview/wywiad-z-die-flote-off-festival-2014/',
                         '/interview/wywiad-z-kaseciarzem-off-festival-2014/',
                         '/interview/wywiad-z-kobietami-off-festival-2014/', '/interview/wywiad-z-kixnarem/',
                         '/interview/wywiad-z-dizzeem-rascalem/', '/interview/wywiad-z-inc/',
                         '/interview/wywiad-z-manuelem-gottschingiem/', '/interview/wywiad-z-deonem/',
                         '/interview/wywiad-z-teengirl-fantasy/', '/interview/wywiad-z-kuedo/',
                         '/interview/wywiad-z-petem-adamsem-gitarzysta-baroness/',
                         '/interview/wywiad-z-natem-newtonem-z-converge/', '/interview/wywiad-z-maolatem-i-pezetem/',
                         '/interview/wywiad-z-baxterem/', '/interview/wywiad-z-ramona-rey-i-igorem-czerniawskim/',
                         '/interview/wywiad-z-neon-indian/'],
    'article_list': '/article/',
    'article_detail': ['/article/porcys-guide-festival-2015/', '/article/porcys-guide-spring-break-2015/',
                       '/article/porcys-guide-red-bull-music-academy-weekender-2015/',
                       '/article/not-all-men-rap-mixtejp-vol-1/', '/article/porcys-guide-to-off-festival-2014/',
                       '/article/porcys-guide-to-red-bull-music-academy-weekender-1/',
                       '/article/30-najlepszych-piosenek-pet-shop-boys/',
                       '/article/a-very-special-christmas-mini-przewodnik-po-dyskog/',
                       '/article/30-najlepszych-utworow-madonny/', '/article/20-najlepszych-utworow-hey-i-nosowskiej/',
                       '/article/20-najlepszych-utworow-kraftwerk/', '/article/r-stevie-moore-w-50-odsonach/',
                       '/article/10-najlepszych-polskich-producentow-hiphopowych/',
                       '/article/20-najlepszych-polskich-raperow-wszech-czasow/',
                       '/article/najlepsi-polscy-raperzy-poczekalnia/',
                       '/article/porcys-darling-wannabes-50-nowych-artystow/',
                       '/article/porcys-darling-wannabes-honorable-mention/',
                       '/article/20-najlepszych-piosenek-the-cardigans/',
                       '/article/posowie-jeszcze-raz-o-podsumowaniu-lat-90-tych/', '/article/rytuay-i-nieporozumienia/',
                       '/article/10-najlepszych-piosenek-tlc/', '/article/20-najlepszych-piosenek-outkast/',
                       '/article/najlepsze-recenzje-w-historii-serwisu/', '/article/z-archiwum-skrzynki-pocztowej/',
                       '/article/20-najlepszych-piosenek-the-smiths/', '/article/10-najlepszych-piosenek-aaliyah/',
                       '/article/the-road-to-anagramowe-sekwencje-the-complete-galv/',
                       '/article/rekomendacje-plytowe-2000-2009/',
                       '/article/100-najlepszych-plyt-krzysztofa-krawczyka/',
                       '/article/rok-w-absurdach-2009/'],
    'relation_list': '/relation/',
    'relation_detail': ['/relation/relacja-unsound-2015-surprise/', '/relation/relacja-festival-2015/',
                        '/relation/relacja-kody-festiwal-2015/', '/relation/festiwal-transwizje-6/',
                        '/relation/fotorelacja-off-festival-2014/', '/relation/relacja-off-festival-2014/',
                        '/relation/rbma-weekender-2014/',
                        '/relation/relacja-knower-31-sierpnia-blue-whale-los-angeles/',
                        '/relation/relacja-sonar-2013/', '/relation/inc-25-pazdziernika-2012-los-globos-los-angeles/',
                        '/relation/relacja-off-festival-2012/', '/relation/relacja-pop-montreal/',
                        '/relation/relacja-ice-choir-release-party/', '/relation/various-live-2011/',
                        '/relation/100-dziesiate-urodziny-porcys/', '/relation/relacja-hip-hop-kemp-2011/',
                        '/relation/relacja-san-miguel-primavera-sound-2011/', '/relation/various-live-2010/',
                        '/relation/various-live-2009/', '/relation/relacja-primavera-sound-festival/',
                        '/relation/relacja-off-festival-2009/', '/relation/various-live-2008/',
                        '/relation/off-festival-2008/', '/relation/heineken-opener-festival-2008/',
                        '/relation/festiwal-nowa-muzyka-2008/', '/relation/various-live-2007/',
                        '/relation/heineken-opener-festival-2007/', '/relation/various-live-2006/', '/relation/wrens/',
                        '/relation/various-live-2005/'],
    'rubric_list': '/rubric/',
    'rubric_detail': ['/rubric/orient-express-4/', '/rubric/paradne-swiadoma-i-dojrzala/', '/rubric/orient-express-3/',
                      '/rubric/rekapitulacja-2015-rip/', '/rubric/rekapitulacja-2015-pop/',
                      '/rubric/rekapitulacja-2015-hip-hop/', '/rubric/rekapitulacja-2015-avant-experimental/',
                      '/rubric/rekapitulacja-2015-elektronika/', '/rubric/rekapitulacja-2015-metal/',
                      '/rubric/rekomendacje-plytowe-2015/', '/rubric/rekomendacje-singlowe-2015/',
                      '/rubric/rekapitulacja-2015-indie/', '/rubric/nuty-dzwieki-2015/',
                      '/rubric/rekapitulacja-2015-azja/', '/rubric/porcys-skladak-listopad-2015/',
                      '/rubric/porcys-skladak-pazdziernik-2015/',
                      '/rubric/playlist-extra-10-najlepszych-utworow-godspeed-you/', '/rubric/orient-express-2/',
                      '/rubric/dillpack-1/', '/rubric/playlist-extra-unsound-2015-surprise/',
                      '/rubric/porcys-skladak-wrzesien-2015/', '/rubric/orient-express-1/',
                      '/rubric/porcys-skladak-sierpien-2015/', '/rubric/porcys-skladak-lipiec-2015/',
                      '/rubric/playlist-extra-15-najlepszych-utworow-lso/',
                      '/rubric/na-spytki-cie-biorac-afro-kolektyw/',
                      '/rubric/porcys-skladak-watergate/', '/rubric/porcys-skladak-fryderyk-chopin/',
                      '/rubric/porcys-skadak-walentynki-2015/', '/rubric/rekapitulacja-2014-elektronika/'],
    'news_list': '/news/2016/03/',
    'news_detail': ['/news/unikatowy-winyl-boards-canada-do-kupienia-na-disco/',
                    '/news/albumy-z-serii-polish-jazz-znow-w-sprzedazy/',
                    '/news/notorious-big-powroci-jako-hologram/',
                    '/news/las-vegas-odwraca-sie-od-edm-u-avicii-wiecej-nie-z/',
                    '/news/beethoven-wygrywa-z-mozartem/',
                    '/news/szczegoly-nowego-albumu-reni-jusis/',
                    '/news/winyle-przynosza-wieksze-dochody-niz-serwisy-strea/',
                    '/news/paul-mccartney-w-nowej-czesci-piratow-z-karaibow/',
                    '/news/flying-lotus-ze-swiatecznymi-prezentami/',
                    '/news/ariel-pink-na-prezydenta/',
                    '/news/alunageorge-na-jedynym-koncercie-w-minecrafcie/',
                    '/news/wspolzalozyciel-tribe-called-quest-nie-zyje/',
                    '/news/prince-szykuje-autobiografie/',
                    '/news/nowy-album-niecheci-juz-w-kwietniu/',
                    '/news/young-thug-wyjdzie-z-trumny-juz-w-piatek/',
                    '/news/nadchodzi-plyta-pro8l3mu/',
                    '/news/jack-lowden-zagra-morrisseya/',
                    '/news/album-na-czesc-grateful-dead-juz-w-maju/',
                    '/news/vince-staples-rowniez-nie-przepada-za-serwisem-spo/',
                    '/news/nowy-album-drake-prawdopodobnie-juz-w-kwietniu/',
                    '/news/spadkobiercy-michaela-jacksona-sprzedaja-prawa-do-/',
                    '/news/hudson-mohawke-czuje-sie-oszukany/',
                    '/news/krazek-radiohead-na-dniach/',
                    '/news/plyta-z-udzialem-zbigniewa-stonogi-do-kupienia-na-/',
                    '/news/novation-podpowiada-jak-zakonczyc-swoj-utwor/',
                    '/news/black-editions-szykuje-uczte-dla-fanow-japonskiej-/',
                    '/news/wilco-w-czerwcu-w-bialymstoku/',
                    '/news/mamy-spodziewac-sie-nowej-wersji-life-pablo/',
                    '/news/szykuja-sie-ciekawe-premiery-w-holdzie-milesowi-da/',
                    '/news/czy-koniec-darmowego-soundclouda/',
                    '/news/porcys-w-telewizji-sniadaniowej/',
                    '/news/george-martin-nie-zyje/',
                    '/news/jessy-lanza-wystapi-na-warszawskim-rbma-weekender/',
                    '/news/final-konkursu-na-mural-poswiecony-davidowi-bowiem/',
                    '/news/pomoz-wydac-nowy-album-lindy-perhacs/',
                    '/news/reni-jusis-wraca-do-gry/',
                    '/news/bjork-znow-wspolpracuje-z-arca/',
                    '/news/kamasi-washington-szykuje-komiks/',
                    '/news/nowy-album-kendricka-lamara-wrzucony-do-sieci/',
                    '/news/kolejny-posmiertny-album-jeffa-buckleya/',
                    '/news/dj-earl-oraz-oneohtrix-point-never-lacza-sily/',
                    '/news/nowy-material-kendricka-lamara-jeszcze-w-tym-roku/',
                    '/news/chaz-bundick-pod-obiektywem/',
                    '/news/m83-z-nowa-plyta-juz-w-kwietniu/'],
    'event_list': '/event/2016/03/',
    'event_detail': ['/event/world-wide-warsaw/', '/event/world-wide-warsaw/', '/event/world-wide-warsaw/',
                     '/event/world-wide-warsaw/', '/event/bts-female-edition/', '/event/bts-female-edition/',
                     '/event/bts-female-edition/', '/event/bts-female-edition/', '/event/petite-noir-w-hybrydach/',
                     '/event/petite-noir-w-hybrydach/', '/event/petite-noir-w-hybrydach/',
                     '/event/petite-noir-w-hybrydach/', '/event/king-kong-b-day-feat-africaine-808-nomad/',
                     '/event/king-kong-b-day-feat-africaine-808-nomad/',
                     '/event/king-kong-b-day-feat-africaine-808-nomad/',
                     '/event/king-kong-b-day-feat-africaine-808-nomad/', '/event/baroness-w-proximie/',
                     '/event/baroness-w-proximie/', '/event/baroness-w-proximie/', '/event/baroness-w-proximie/',
                     '/event/helm-w-pardon-tu/', '/event/helm-w-pardon-tu/', '/event/helm-w-pardon-tu/',
                     '/event/helm-w-pardon-tu/', '/event/5-x-brotzmann-w-pardon-tu/',
                     '/event/5-x-brotzmann-w-pardon-tu/',
                     '/event/5-x-brotzmann-w-pardon-tu/', '/event/5-x-brotzmann-w-pardon-tu/',
                     '/event/xenony-w-trasie/',
                     '/event/xenony-w-trasie/', '/event/xenony-w-trasie/', '/event/xenony-w-trasie/'],
    'supplement_list': '/supplement/2015/11/',
    'supplement_detail': ['/supplement/2015/12/', '/supplement/vancouver-nights-naikoon-park/',
                          '/supplement/vancouver-nights-naikoon-park/', '/supplement/vancouver-nights-naikoon-park/',
                          '/supplement/celine-dion-because-you-loved-me/',
                          '/supplement/celine-dion-because-you-loved-me/',
                          '/supplement/celine-dion-because-you-loved-me/', '/supplement/eric-serra-experience-love/',
                          '/supplement/eric-serra-experience-love/', '/supplement/eric-serra-experience-love/',
                          '/supplement/neu-danzing/', '/supplement/neu-danzing/', '/supplement/neu-danzing/'],
    'guest_list': '/guests/',
    'guest_detail': ['/guests/kylie-minogue-better-devil-you-know/',
                     '/guests/notorious-big-ft-bone-thugs-notorious-thugs/',
                     '/guests/pianohooligan-study-14/',
                     '/guests/pinkcourtesyphone-gwyneth-wentink-elision/'],
    'forum_list': '/forum/',
    'letter_list': '/letter/',
    'letter_detail': ['/letter/ejzee/', '/letter/nin-2/', '/letter/uwag-kilka/', '/letter/nin/',
                      '/letter/post-regiment/',
                      '/letter/with-teeth/', '/letter/-2/', '/letter/zastanawiam-sie/', '/letter/smrod/',
                      '/letter/dotyczy-nowej-pyty-brmc/', '/letter/pochwalony/', '/letter/cenzura-na-forum/',
                      '/letter/portal/', '/letter/isis/', '/letter/porcys-3/', '/letter/nbsp-25/',
                      '/letter/o-myslovitzowych-skalarach-i-sztuce-recenzowania/', '/letter/bu-hahaha/',
                      '/letter/ty-takze-mozesz-zasugerowac-nam-temat-do-playlistu/', '/letter/nbsp-24/',
                      '/letter/signify/', '/letter/felieton-widziaem-eve-herzigova-czyli-wizyta-w/',
                      '/letter/kolejna-promocja-na-vivid-i-merlin/', '/letter/hej-2/', '/letter/mail-w-stylu-jarmusha/',
                      '/letter/nbsp-23/', '/letter/huj-ci-w-leb-za-jerrego-cantrella/',
                      '/letter/do-michala-idioty-swiniojebcy-i-gownolicegopedaa/', '/letter/jerry-cantrell/',
                      '/letter/obrona-zdania-ludzi-posiadajacych-gust-muzyczny/'],
}


class UserBehavior(TaskSet):
    """
    Class represents behavior of single user on the webpage
    """

    def __init__(self, *args, **kwargs):
        super(UserBehavior, self).__init__(*args, **kwargs)
        if 'updated' not in URLS and False:
            # update urls for detail views
            URLS.update({
                'updated': True,
                'review_detail': self.get_urls('review_list', '.slider-content  li  a'),
                'playlist_detail': self.get_urls('playlist_list', '.slider-content  li  a'),
                # 'short_album_detail': self.get_urls('short_album_list', '.shorty  li  a'),
                'short_song_detail': self.get_urls('short_song_list', '.shorty  li  a'),
                'draft_detail': self.get_urls('draft_list', '.simple li ul li a'),
                'ranking_detail': self.get_urls('ranking_list', '.simple li ul li a'),
                'interview_detail': self.get_urls('interview_list', '.simple li ul li a'),
                'article_detail': self.get_urls('article_list', '.simple li ul li a'),
                'relation_detail': self.get_urls('relation_list', '.simple li ul li a'),
                'rubric_detail': self.get_urls('rubric_list', '.simple li ul li a'),
                'news_detail': self.get_urls('news_list', '.simple  li ul li a'),
                'event_detail': self.get_urls('event_list', '.events li a'),
                'supplement_detail': self.get_urls('supplement_list', '.supplement li a'),
                'guest_detail': self.get_urls('guest_list', '.guests li a'),
                # 'forum_detail': self.get_urls('forum_list', '.simple li ul li a'),
                'letter_detail': self.get_urls('letter_list', '.simple li ul li a'),
            })
            with open('urls.json', 'w') as ff:
                ff.write(str(URLS))

    def get_urls(self, url_name, selector):
        """
        Get content of the page using requests and return anchors for a given jquery selector.
        """
        # import jquery-like library for python
        from pyquery import PyQuery
        # import http requests
        import requests
        response = requests.get(self.locust.host + URLS[url_name])
        d = PyQuery(response.content)
        return [PyQuery(element).attr.href for element in d(selector)]

    @task(1715)
    def index(self):
        self.client.get(URLS['index'])

    @task(990)
    def review_list(self):
        self.client.get(URLS['review_list'])

    @task(990)
    def review_detail(self):
        return self.client.get(random.choice(URLS['review_detail']), name='/review/[slug]/')

    @task(300)
    def playlist_list(self):
        self.client.get(URLS['playlist_list'])

    @task(300)
    def playlist_detail(self):
        return self.client.get(random.choice(URLS['playlist_detail']), name='/playlist/[slug]/')

    @task(276)
    def short_album_list(self):
        self.client.get(URLS['short_album_list'])

    # @task(276)
    # def short_album_detail(self):
    #     return self.client.get(random.choice(URLS['short_album_detail']), name='/short/album/[slug]/')

    @task(552)
    def short_song_list(self):
        self.client.get(URLS['short_song_list'])

    @task(276)
    def short_song_detail(self):
        return self.client.get(random.choice(URLS['short_song_detail']), name='/short/song/[slug]/')

    @task(50)
    def draft_list(self):
        self.client.get(URLS['draft_list'])

    @task(50)
    def draft_detail(self):
        return self.client.get(random.choice(URLS['draft_detail']), name='/draft/[slug]/')

    @task(340)
    def ranking_list(self):
        self.client.get(URLS['ranking_list'])

    @task(340)
    def ranking_detail(self):
        return self.client.get(random.choice(URLS['ranking_detail']), name='/ranking/[slug]/')

    @task(7)
    def interview_list(self):
        self.client.get(URLS['interview_list'])

    @task(7)
    def interview_detail(self):
        return self.client.get(random.choice(URLS['interview_detail']), name='/interview/[slug]/')

    @task(690)
    def article_list(self):
        self.client.get(URLS['article_list'])

    @task(690)
    def article_detail(self):
        return self.client.get(random.choice(URLS['article_detail']), name='/article/[slug]/')

    @task(2)
    def relation_list(self):
        self.client.get(URLS['relation_list'])

    @task(2)
    def relation_detail(self):
        return self.client.get(random.choice(URLS['relation_detail']), name='/relation/[slug]/')

    @task(94)
    def rubric_list(self):
        self.client.get(URLS['rubric_list'])

    @task(94)
    def rubric_detail(self):
        return self.client.get(random.choice(URLS['rubric_detail']), name='/rubric/[slug]/')

    @task(592)
    def news_list(self):
        self.client.get(URLS['news_list'])

    @task(592)
    def news_detail(self):
        return self.client.get(random.choice(URLS['news_detail']), name='/news/[slug]/')

    @task(20)
    def event_list(self):
        self.client.get(URLS['event_list'])

    @task(20)
    def event_detail(self):
        return self.client.get(random.choice(URLS['event_detail']), name='/event/[slug]/')

    @task(22)
    def supplement_list(self):
        self.client.get(URLS['supplement_list'])

    @task(22)
    def supplement_detail(self):
        return self.client.get(random.choice(URLS['supplement_detail']), name='/supplement/[slug]/')

    @task(2)
    def guest_list(self):
        self.client.get(URLS['guest_list'])

    @task(18)
    def guest_detail(self):
        return self.client.get(random.choice(URLS['guest_detail']), name='/guest/[slug]/')

    @task(228)
    def forum_list(self):
        self.client.get(URLS['forum_list'])

    # @task(228)
    # def forum_detail(self):
    #     return self.client.get(random.choice(URLS['forum_detail']), name='/forum/[slug]/')

    @task(2)
    def letter_list(self):
        self.client.get(URLS['letter_list'])

    @task(2)
    def letter_detail(self):
        return self.client.get(random.choice(URLS['letter_detail']), name='/letter/[slug]/')


class WebsiteUser(HttpLocust):
    """
    Locust class, that uses behavior class.
    """
    task_set = UserBehavior
    # minimum and maximum time between "clicks"
    min_wait = 5000
    max_wait = 9000
